(function($) {
	$(document).ready(function () {
		$('.dropdown').on('show.bs.dropdown', function () {
			$(this).siblings('.open').removeClass('open').find('a.dropdown-toggle').attr('data-toggle', 'dropdown');
			$(this).find('a.dropdown-toggle').removeAttr('data-toggle');
		});
		$('.shows').find('.show-info').on('click', function() {
			$(this).siblings('.more-info').slideToggle(300);
		});
		$('.photo-gallery').find('img').each(function(){
			$(this).css({'margin-left': -($(this).width() / 2), 'position': 'absolute', 'left': '50%'});
		});
	});
})(jQuery);